package dublduk.mycult.server.db

import org.janusgraph.core.JanusGraph
import org.janusgraph.core.JanusGraphFactory

object InMemGraphDb {
    val graph: JanusGraph by lazy(LazyThreadSafetyMode.SYNCHRONIZED) {
        val janusGraph: JanusGraph = JanusGraphFactory.open("inmemory")
        janusGraph
    }
}
