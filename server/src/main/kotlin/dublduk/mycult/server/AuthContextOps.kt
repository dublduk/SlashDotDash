package dublduk.mycult.server

import dublduk.mycult.data.TokenData
import dublduk.mycult.server.security.Token
import io.javalin.http.Context
import io.javalin.http.Cookie
import io.javalin.http.HttpCode
import kotlinx.serialization.json.Json

private const val SET_COOKIE_HEADER_KEY = "Set-Cookie"
private const val AUTHORIZATION_HEADER_KEY = "Authorization"
private const val WWW_AUTHENTICATE_HEADER_KEY = "WWW-Authenticate"
private const val BEARER_STRING = "Bearer"
private const val APP_USER_AGENT_VALUE = "MyCult Application"

fun Context.findAccessToken(): String? {
    return findTokenByKey(Token.ACCESS_TOKEN_KEY)
}

fun Context.findRefreshToken(): String? {
    return findTokenByKey(Token.REFRESH_TOKEN_KEY)
}

private fun Context.findTokenByKey(tokenKey: String): String? {
    if (isApplicationRequest()) {
        return header(AUTHORIZATION_HEADER_KEY)?.substring(BEARER_STRING.length + 1)?.trim()
    } else { // browser request
        return cookie(tokenKey)
    }
}

fun Context.sendTokens(accessToken: Token, refreshToken: Token) {
    if (isApplicationRequest()) {
        result(Json.encodeToString(TokenData.serializer(), TokenData(accessToken.value, refreshToken.value)))
    } else { // browser request
        sendTokenCookies(accessToken, refreshToken)
    }
}

fun Context.sendUnauthorized() {
    status(HttpCode.UNAUTHORIZED).header(WWW_AUTHENTICATE_HEADER_KEY, BEARER_STRING)
}

fun Context.getUserId(): String {
    return sessionAttribute<String>("userId") ?: throw IllegalStateException("Allowed only for authorized users.")
}

private fun Context.isApplicationRequest(): Boolean = userAgent() == APP_USER_AGENT_VALUE

private fun Context.sendTokenCookies(accessToken: Token, refreshToken: Token) {
    res.addHeader(SET_COOKIE_HEADER_KEY, renderCookie(accessToken))
    res.addHeader(SET_COOKIE_HEADER_KEY, renderCookie(refreshToken))
    cookie(Cookie("logged_in", "true", maxAge = accessToken.maxAge, secure = true))
}

private fun renderCookie(token: Token) = "${token.name}=${token.value}; Secure; HTTPOnly; Max-Age=${token.maxAge}"
