package dublduk.mycult.server

import dublduk.mycult.server.security.TokenGenerator
import io.javalin.core.security.AccessManager
import io.javalin.core.security.RouteRole
import io.javalin.http.Context
import io.javalin.http.Handler

class UserAccessManager(private val tokenGenerator: TokenGenerator) : AccessManager {
    override fun manage(handler: Handler, ctx: Context, routeRoles: MutableSet<RouteRole>) {
        when {
            Role.ANYONE in routeRoles -> {
                handler.handle(ctx)
            }
            Role.USER in routeRoles -> {
                val accessToken = ctx.findAccessToken()
                if (accessToken != null) {
                    val userId = tokenGenerator.verifyAccessToken(accessToken)
                    ctx.sessionAttribute("userId", userId)
                    handler.handle(ctx)
                } else {
                    ctx.sendUnauthorized()
                }
            }
            else -> {
                throw IllegalStateException("The role should be defined for the'${ctx.contextPath()}' path.")
            }
        }
    }
}

enum class Role : RouteRole { ANYONE, USER }
