package dublduk.mycult.server

import com.typesafe.config.ConfigFactory
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.Serializable
import kotlinx.serialization.hocon.Hocon
import java.net.URL

@ExperimentalSerializationApi
fun loadServerConfig(): ServerConfig {
    val config = ConfigFactory.load("server")
    return Hocon.decodeFromConfig(ServerConfig.serializer(), config)
}

@Serializable
data class ServerConfig(val deployment: DeploymentConfig, val auth: AuthConfig, val files: FilesConfig)

@Serializable
data class DeploymentConfig(val host: String, val port: Int)

@Serializable
data class AuthConfig(val accessTokenHours: Int, val refreshTokenDays: Int)

@Serializable
data class FilesConfig(val name: String, val uploadTo: String)

fun DeploymentConfig.toUrlString(): String {
    return URL("http", host, port, "/").toExternalForm()
}
