package dublduk.mycult.server

import dublduk.mycult.data.UserData
import dublduk.mycult.server.repo.SessionRepository
import dublduk.mycult.server.repo.UserRepository
import dublduk.mycult.server.security.PswdHasher
import dublduk.mycult.server.security.Token
import dublduk.mycult.server.security.TokenGenerator

class UserManagement(
    private val userRepository: UserRepository,
    private val sessionRepository: SessionRepository,
    private val pswdHasher: PswdHasher,
    private val tokenGenerator: TokenGenerator
) {
    fun createUser(username: String, pswd: String): Pair<Token, Token> {
        val pswdHash = pswdHasher.hash(pswd)
        val userId = userRepository.add(username, pswdHash)
        val tokens = tokenGenerator.generateTokens(userId)
        sessionRepository.add(userId, tokens.second.value)
        return tokens
    }

    fun login(username: String, pswd: String): Pair<Token, Token> {
        val user = userRepository.findByName(username)
        if (user != null) {
            if (pswdHasher.verify(user.pswdHash, pswd)) {
                val tokens = tokenGenerator.generateTokens(user.id)
                sessionRepository.add(user.id, tokens.second.value)
                return tokens
            } else {
                throw WrongPasswordException("It's so wrong.")
            }
        } else {
            throw UserNotFoundException("User with username '$username' not found.")
        }
    }

    fun refreshTokens(oldRefreshToken: String): Pair<Token, Token> {
        tokenGenerator.verifyRefreshToken(oldRefreshToken)
        val userId = sessionRepository.findUserIdByToken(oldRefreshToken)
        if (userId != null) {
            val tokens = tokenGenerator.generateTokens(userId)
            sessionRepository.replace(userId, oldRefreshToken, tokens.second.value)
            return tokens
        } else {
            throw BadTokenException("Refresh token expired.")
        }
    }

    fun userSettings(userId: String): UserData {
        val user = userRepository.findById(userId)
        if (user != null) {
            return UserData(user.username)
        } else {
            throw UserNotFoundException("User with ID '$userId' not found.")
        }
    }
}

class UserNotFoundException(message: String) : RuntimeException(message)

class WrongPasswordException(message: String) : RuntimeException(message)

class BadTokenException(message: String) : RuntimeException(message)
