package dublduk.mycult.server.repo

import java.util.concurrent.ConcurrentHashMap

private val REFRESH_TOKENS = ConcurrentHashMap<String, String>()

class InMemSessionRepository : SessionRepository {
    override fun add(userId: String, refreshToken: String) {
        REFRESH_TOKENS[refreshToken] = userId
    }

    override fun findUserIdByToken(refreshToken: String): String? {
        return REFRESH_TOKENS[refreshToken]
    }

    override fun replace(userId: String, oldRefreshToken: String, newRefreshToken: String) {
        REFRESH_TOKENS.remove(oldRefreshToken)
        REFRESH_TOKENS[newRefreshToken] = userId
    }
}
