package dublduk.mycult.server.repo

import dublduk.mycult.server.model.User
import org.apache.tinkerpop.gremlin.process.traversal.TraversalSource
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversalSource
import org.apache.tinkerpop.gremlin.structure.Vertex

class GUserRepository(private val g: GraphTraversalSource) : UserRepository {
    override fun add(username: String, pswd: String): String {
        val tx = g.tx()
        tx.begin<TraversalSource>()
        val v = g.addV(User.MODEL_KEY)
            .property(User.USERNAME_KEY, username)
            .property(User.PSWD_KEY, pswd)
            .next()
        tx.commit()
        return (v.id() as Long).toString()
    }

    override fun findById(id: String): User? {
        return g.V().hasLabel(User.MODEL_KEY).hasId(id.toLong()).tryNext()
            .map { vToUser(it) }
            .orElse(null)
    }

    override fun findByName(username: String): User? {
        return g.V().hasLabel(User.MODEL_KEY).has(User.USERNAME_KEY, username).tryNext()
            .map { vToUser(it) }
            .orElse(null)
    }

    override fun renameUser(id: String, newUsername: String): User {
        val tx = g.tx()
        tx.begin<TraversalSource>()
        val v = g.V().hasLabel(User.MODEL_KEY).hasId(id).property(User.USERNAME_KEY, newUsername).next()
        tx.commit()
        return vToUser(v)
    }

    override fun listAll(): List<User> {
        return g.V().hasLabel(User.MODEL_KEY)
            .map { v -> v.get()?.let { vToUser(it) } }
            .toList()
    }

    companion object {
        private fun vToUser(v: Vertex): User {
            val id = (v.id() as Long).toString()
            val username = v.property<String>(User.USERNAME_KEY).value()
            val pswd = v.property<String>(User.PSWD_KEY).value()
            return User(id, username, pswd)
        }
    }
}
