package dublduk.mycult.server.security

data class HashConfig(
    val saltLength: Int = 16,
    val hashLength: Int = 32,
    val iterations: Int = 10,
    val memory: Int = 64, // kibibytes
    val parallelism: Int = 1
)
