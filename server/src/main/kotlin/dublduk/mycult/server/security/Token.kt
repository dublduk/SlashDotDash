package dublduk.mycult.server.security

data class Token(val name: String, val value: String, val maxAge: Int) {
    companion object {
        const val ACCESS_TOKEN_KEY = "access_token"
        const val REFRESH_TOKEN_KEY = "refresh_token"

        fun createAccessToken(value: String, maxAge: Int): Token {
            return Token(ACCESS_TOKEN_KEY, value, maxAge)
        }

        fun createRefreshToken(value: String, maxAge: Int): Token {
            return Token(REFRESH_TOKEN_KEY, value, maxAge)
        }
    }
}
