package dublduk.mycult.server.security

import io.jsonwebtoken.ExpiredJwtException
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import io.jsonwebtoken.security.Keys
import kotlinx.datetime.Clock
import kotlinx.datetime.Instant
import java.util.*
import javax.crypto.SecretKey

private const val USER_ID_KEY = "userId"

class JwtGenerator(private val tokenConfig: TokenConfig) : TokenGenerator {
    private var secretKey: SecretKey = Keys.secretKeyFor(SignatureAlgorithm.HS256) // or HS384 or HS512

    override fun generateTokens(userId: String): Pair<Token, Token> {
        val now = Clock.System.now()
        val accessTokenExpiration = now.plus(tokenConfig.accessTime)
        val accessToken = Jwts.builder()
            .setIssuedAt(now.toJavaDate())
            .setExpiration(accessTokenExpiration.toJavaDate())
            .setAudience(tokenConfig.audience)
            .setIssuer(tokenConfig.issuer)
            .claim(USER_ID_KEY, userId)
            .signWith(secretKey)
            .compact()
        val refreshTokenExpiration = accessTokenExpiration.plus(tokenConfig.refreshTime)
        val refreshToken = Jwts.builder()
            .setIssuedAt(now.toJavaDate())
            .setExpiration(refreshTokenExpiration.toJavaDate())
            .setAudience(tokenConfig.audience)
            .setIssuer(tokenConfig.issuer)
            .signWith(secretKey)
            .compact()
        return Pair(
            Token.createAccessToken(accessToken, tokenConfig.accessTime.inWholeSeconds.toInt()),
            Token.createRefreshToken(refreshToken, tokenConfig.refreshTime.inWholeSeconds.toInt())
        )
    }

    override fun verifyAccessToken(accessToken: String): String {
        val parser = Jwts.parserBuilder()
            .setSigningKey(secretKey)
            .build()
        try {
            return parser.parseClaimsJwt(accessToken).body[USER_ID_KEY] as String
        } catch (e: ExpiredJwtException) {
            throw TokenExpiredException(e)
        }
    }

    override fun verifyRefreshToken(refreshToken: String) {
        val parser = Jwts.parserBuilder()
            .setSigningKey(secretKey)
            .build()
        try {
            parser.parse(refreshToken)
        } catch (e: ExpiredJwtException) {
            throw TokenExpiredException(e)
        }
    }
}

fun Instant.toJavaDate(): Date {
    return Date(toEpochMilliseconds())
}
