package dublduk.mycult.server.security

interface PswdHasher {
    fun hash(pswd: String): String

    fun verify(pswdHash: String, candidate: String): Boolean
}
