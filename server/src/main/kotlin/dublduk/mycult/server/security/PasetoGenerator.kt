package dublduk.mycult.server.security

import dev.paseto.jpaseto.ExpiredPasetoException
import dev.paseto.jpaseto.Pasetos
import dev.paseto.jpaseto.lang.Keys
import kotlinx.datetime.Clock
import kotlinx.datetime.toJavaInstant
import javax.crypto.SecretKey

private const val USER_ID_KEY = "userId"

class PasetoGenerator(private val tokenConfig: TokenConfig) : TokenGenerator {
    private val secretKey: SecretKey = Keys.secretKey()

    override fun generateTokens(userId: String): Pair<Token, Token> {
        val now = Clock.System.now()
        val accessTokenExpiration = now.plus(tokenConfig.accessTime)
        val accessToken = Pasetos.V2.LOCAL.builder()
            .setSharedSecret(secretKey)
            .setIssuedAt(now.toJavaInstant())
            .setExpiration(accessTokenExpiration.toJavaInstant())
            .setAudience(tokenConfig.audience)
            .setIssuer(tokenConfig.issuer)
            .claim(USER_ID_KEY, userId)
            .compact()
        val refreshTokenExpiration = accessTokenExpiration.plus(tokenConfig.refreshTime)
        val refreshToken = Pasetos.V2.LOCAL.builder()
            .setSharedSecret(secretKey)
            .setIssuedAt(now.toJavaInstant())
            .setExpiration(refreshTokenExpiration.toJavaInstant())
            .setAudience(tokenConfig.audience)
            .setIssuer(tokenConfig.issuer)
            .compact()
        return Pair(
            Token.createAccessToken(accessToken, tokenConfig.accessTime.inWholeSeconds.toInt()),
            Token.createRefreshToken(refreshToken, tokenConfig.refreshTime.inWholeSeconds.toInt())
        )
    }

    override fun verifyAccessToken(accessToken: String): String {
        val parser = Pasetos.parserBuilder()
            .setSharedSecret(secretKey)
            .build()
        try {
            return parser.parse(accessToken).claims[USER_ID_KEY] as String
        } catch (e: ExpiredPasetoException) {
            throw TokenExpiredException(e)
        }
    }

    override fun verifyRefreshToken(refreshToken: String) {
        val parser = Pasetos.parserBuilder()
            .setSharedSecret(secretKey)
            .build()
        try {
            parser.parse(refreshToken)
        } catch (e: ExpiredPasetoException) {
            throw TokenExpiredException(e)
        }
    }
}
