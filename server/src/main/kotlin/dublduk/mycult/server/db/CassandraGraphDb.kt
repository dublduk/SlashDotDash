package dublduk.mycult.server.db

import org.janusgraph.core.JanusGraph
import org.janusgraph.core.JanusGraphFactory

object CassandraGraphDb {
    val graph: JanusGraph by lazy(LazyThreadSafetyMode.SYNCHRONIZED) {
        val janusGraph: JanusGraph = JanusGraphFactory.build()
            .set("storage.backend", "cql")
            .open()
        janusGraph
    }
}
