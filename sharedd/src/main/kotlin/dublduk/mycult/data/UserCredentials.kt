package dublduk.mycult.data

import kotlinx.serialization.Serializable

@Serializable
data class UserCredentials(val username: String, val pswd: String)
